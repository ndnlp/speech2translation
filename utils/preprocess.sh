#!/bin/bash

workd=data/Griko-Italian

# Extract PLP features with feacalc
WAVPATH=$workd/raw
PLPPATH=$workd/plp

mkdir $PLPPATH
python extract_plp.py -i $WAVPATH -o $PLPPATH -t 16

# Extract candidate boundaries with khanaga
BOUNDPATH=$workd/bounds

mkdir $BOUNDPATH
python extract_boundaries.py -i $WAVPATH -o $BOUNDPATH -t 16

# Extract candidate silences
SILPATH=$workd/silences
mkdir $SILPATH
python extract_silences.py -i $WAVPATH -o $SILPATH -t 16

