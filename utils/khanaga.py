import numpy as np
import scipy.io.wavfile as wav
import argparse
#import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
from scipy.stats import norm
from scipy.stats import chisqprob

# Functions for low-pass filter
def butter_lowpass(cutoff, fs, order=5):
	nyq = 0.5 * fs
	normal_cutoff = cutoff / nyq
	b, a = butter(order, normal_cutoff, btype='low', analog=False)
	return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
	b, a = butter_lowpass(cutoff, fs, order=order)
	y = lfilter(b, a, data)
	return y

# Compute ACC
def compute_ACC(sig, rate):
	Gamma_mu_0 = np.zeros([len(sig)])
	Gamma_mu_1 = np.zeros([len(sig)])
	Gamma_rho_kappa = np.zeros([len(sig)])
	kappa_tau = np.zeros([len(sig)])
	rho_0 = 1.0/rate

	for n in range(1,len(sig)):
		Gamma_mu_0[n] = np.abs(int(sig[n])-int(sig[n-1]))
		if (Gamma_mu_0[n] == 0):
			Gamma_mu_0[n] = 0.0001

	for n in range(1,len(sig)-1):
		Gamma_mu_1[n] = np.abs(int(sig[n])-int(sig[n-1])) + np.abs(int(sig[n+1]) - int(sig[n]))
		if (Gamma_mu_1[n] == 0):
			Gamma_mu_1[n] = 0.0001

	for n in range(1,len(sig)-1):
		kappa_tau[n] = np.sqrt(np.divide(Gamma_mu_1[n],Gamma_mu_0[n]))
		if (kappa_tau[n] == 0):
			kappa_tau[n] = 0.0001

	Gamma_rho_kappa = np.multiply(kappa_tau,Gamma_mu_0)
	Gamma_rho_kappa = Gamma_rho_kappa[1:len(sig)-1]

	temp = np.average(Gamma_mu_0)

	h = np.zeros([len(Gamma_rho_kappa)])
	h = (np.log(Gamma_rho_kappa) - np.log(temp)) / np.log(rho_0)

	h_bar = np.average(h)
	h_help = h - h_bar
	ACC = np.zeros([len(h)-1])
	ACC[0] = h_help[0]
	for n in range(2,len(h)):
		ACC[n-1] = ACC[n-2] + h_help[n-1]
	
	# Normalise ACC to unity
	ACC = ACC / np.max(ACC)
	
	return (h,ACC)

# function to compute MSE
def MSE(sig,x1,x2):
	if len(sig)>0:
		y1 = sig[0]
		y2 = sig[-1]
		slope = float(y2-y1)/float(x2-x1+1)
		#print len(sig), x1, y1, x2, y2, y1-slope*x1, slope
		xs = np.arange(x1,x2)
		lin = slope * xs - slope*x1 + y1
		dif = sig - lin
		return np.mean(np.power(dif,2))
	else:
		return 0.0

# second way to compute MSE
def MSE2(sig,x1,x2):
	x = np.array([x1,x2])
	y = np.array([sig[0],sig[-1]])
	z = np.polyfit(x,y,1)

	xs = np.arange(x1,x2)
	lin = z[1]*xs + z[0]
	dif = sig - lin

	return np.mean(np.power(dif,2))

# PLA algorithm for boundary detection
def PLA(ACC, epsilon):
	N = len(ACC)
	i = 1
	c = [1]
	k1 = 3
	prev = 0
	while (k1 < N):
		if (MSE(ACC[c[i-1]:k1], c[i-1], k1) > epsilon):
			ni = k1
			l = ni - c[i-1]
			E2 = np.empty([l])
			for k2 in range(l):
				E2[k2] = MSE(ACC[c[i-1]:c[i-1]+k2], c[i-1], c[i-1]+k2) + MSE(ACC[c[i-1]+k2:k1], c[i-1]+k2, k1)
			ci = np.argmin(E2) + c[i-1]
			c.append(ci)
			k1 = c[i]
			i += 1
		k1 += 1
		
	return c

# Function to compute some partial performance measures
def partial(bounds, gold, tolerance, rate):
	NR = float(len(gold))
	NT = float(len(bounds))
	window = tolerance * rate / 1000.0 #tolerance is given in ms
	NH = 0.0
	gold2 = list(gold)
	for b in bounds:
		#print b
		for g in gold2[:]:
			if (np.abs(g-b) < window):
				gold2.remove(g)
				NH += 1.0
				print "Correct boundary:", b, "(gold: ", g, ")"
				break
	# Measures
	HR = NH / NR
	OS = (NT - NR) / NR
	FA = (NT - NH) / NT

	return (HR,OS,FA)

# Function to compute LLRT
def LLRT(Z, X, Y):
	
	H0 = np.sum(norm.pdf(Z,np.mean(Z),np.std(Z)))
	H1 = np.sum(norm.pdf(X,np.mean(X),np.std(X))) + np.sum(norm.pdf(Y,np.mean(Y),np.std(Y)))
	Ratio = np.log(H0 / H1)
	chi2 = -2 * Ratio
	#chi2 = -2* np.log(Ratio)
	if (chisqprob(chi2, 1)) < 0.005:
		return False
	else:
		return True

def refineBoundaries(bounds, SE, window):
	c = list(bounds)
	c.sort()
	candidates = [c[0]]
	i = 1
	while (i < len(c)-1):
		if (c[i] - c[i-1] < window):
			c.remove(c[i])
		elif (LLRT(SE[c[i-1]:c[i+1]], SE[c[i-1]:c[i]], SE[c[i]:c[i+1]])):
			candidates.append(c[i])
			i += 1
		else:
			# LLR test showed there shouldn't be a boundary here, do nothing
			c.remove(c[i])
		
	return candidates



def main():

	parser = argparse.ArgumentParser(description='Produce a phonetic segmentation of the given .wav speech file')

	parser.add_argument('-f', nargs=1, required=True, type=str, help='The filename of the .wav (assume a one channel speech signal).')
	parser.add_argument('-e', nargs=1, default=[0.005], help='The value for the epsilon of the PLA procedure.')
	parser.add_argument('-t', nargs=1, default=[10], type=int, help='The tolerance for boundary detection (merges anything closer). Given in milliseconds.')
	parser.add_argument('-o', nargs=1, help='The output file to store the found boundaries.')

	args = parser.parse_args()
	wavfilename = args.f[0]
	epsilon = float(args.e[0])
	tolerance = int(args.t[0])
	
	pr = True
	if not(args.o is None):
		pr = False

	# Read the .wav file
	(rate,sig) = wav.read(wavfilename)

	# Compute ACC on original signal
	(SE1,ACC1) = compute_ACC(sig,rate)

	# Use Lowpass filter
	order = 6
	cutoff = 1800  # desired cutoff frequency of the filter, Hz
	lowsig = butter_lowpass_filter(sig, cutoff, rate, order)

	# Compute ACC on filtered version
	(SE2,ACC2) = compute_ACC(lowsig,rate)

	
	# Find possible boundaries with PLA
	bounds1 = PLA(ACC1, epsilon)
	bounds2 = PLA(ACC2, epsilon)
	# Tolerance for boundary detection
	window = tolerance * rate / 1000.0


	# Get set of found boundaries by both full and low-pass signal
	boundsall = set(bounds1+bounds2)

	# Refine Boundaries with LLRT
	boundsall2 = refineBoundaries(boundsall, SE1, window)

	# Get string list of the found boundaries
	st = ' '.join(map(str, boundsall2))
	if (pr):
		print st
	else:
		outf = open(args.o[0], 'w')
		outf.write(st)
		outf.close()


if __name__ == "__main__":
    main()
