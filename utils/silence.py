#!/usr/bin/env

import numpy as np
import scipy.io.wavfile as wav
import argparse



def main():
    parser = argparse.ArgumentParser(description='Produce a segmentation of the given .wav speech file based on silences.')
    parser.add_argument('-f', nargs=1, required=True, type=str, help='The filename of the .wav (assume a one channel speech signal).')
    parser.add_argument('-c', nargs=1, default=[1], type=int, help='The number of channels in the wav file.')
    parser.add_argument('-t', nargs=1, default=[0.02], type=float, help='The threshold for silence detection.')
    parser.add_argument('-o', nargs=1, type=str, help='The output file to store the found boundaries.')
    
    args = parser.parse_args()
    wavfilename = args.f[0]
    if not(args.c is None):
        ch = float(args.c[0])
        
    threshold = float(args.t[0])
    
    pr = True
    if not(args.o is None):
        pr = False
    
    # Read the .wav file
    (rate,sig) = wav.read(wavfilename)
    if ch==2:
        sig = np.average(sig, axis=1)
        threshold *= 1.5
    
    # Get envelope with abs and moving average
    ab = np.abs(sig)
    mu = 2 * (rate/100)
    env = np.empty([len(ab)-mu])
    for i in range(mu,len(ab)):
        env[i-mu] = np.average(ab[i-mu:i])
    
    #Detect
    av = np.average(env)
    t = env < av * threshold
    bs = []
    prev = 0
    for i,tt in enumerate(t[1:]):
        if tt==1 and prev==0:
            start = i
            prev = 1
        elif tt==0 and prev==1:
            end = i
            prev = 0
            # Divide by 100 to go to 10ms feature range
            bs.append((start/(rate/100),end/(rate/100)))
    if prev==1:
        bs.append((start/(rate/100),len(t)/(rate/100)))

    outf = open(args.o[0], 'w')
    for b in bs:
        outf.write(str(b[0]) + " " + str(b[1]) + "\n")
    outf.close()

if __name__ == "__main__":
    main()
