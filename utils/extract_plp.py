import argparse
import os 
from multiprocessing import Pool

def run_feacalc((inputf,outputf)):
    print 'Extracting plp features for file  : ', inputf
    feacalcpath = '/afs/crc.nd.edu/group/nlp/software/sprachcore/icsi-scenic-tools-20120105/feacalc-0.92/feacalc'
    cmd = feacalcpath + ' -hpfilter 100 -dither -domain cepstra -deltaorder 2 -plp 12 -sr 8000 -opformat ascii -o '+ outputf + ' ' + inputf 
    os.system(cmd)

def read_corpus_wav(fname,fout, ext, threads_no):
    print 'Read corpus from folder ', fname, ' only read file end with ', ext
    all_paths = []
    for file_name in os.listdir(fname): 
        if (file_name.find(ext) == -1): continue
        new_file_name = file_name.replace(ext,'')
        full_path_in = fname + '/' + file_name
        full_path_out = fout + '/' + new_file_name +'.plp'
        all_paths.append((full_path_in,full_path_out))
    pool = Pool(threads_no)
    pool.map(run_feacalc, all_paths)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Calculate PLP features from ')
    
    parser.add_argument('-i','--input', help='Input folder where the audio is stored', required=True)
    parser.add_argument('-o','--output', help='Output PLP folder  ', required=True)
    parser.add_argument('-t','--thread', help='Number of threads', required=False)
    args = parser.parse_args()
    
    no_threads = 1 
    if args.thread != None:
        no_threads = int(args.thread)
    read_corpus_wav(args.input, args.output, '.wav', no_threads)
    
